#include "GDSBuilder.h"
#include <cstdio>


int main(int argc, char* argv[]) {
   // TODO: argv
   GDSBuilder builder = GDSBuilder(10000000, GREEDY);
   builder.setInterventionPoint(10);
   std::printf("Running GDSBuilder using %s.\n\n",
               builder.getAlgorithmName().c_str());
   std::printf("%15s %15s %15s\n\n", "|S|", "max(S)", "lowest diff");
   unsigned int m = 2;
   while (builder.addNext()) {
      if (builder.getSize() == m) {
         std::printf("%15u %15u %15u\n",
	             builder.getSize(),
	             builder.getMaximum(),
		     builder.getLowestUnsaturatedDifference());
         m *= 2;
      }
   }
   std::printf("\nCap %u exceeded; stopping.\n\n", builder.getCap());
   std::printf("%15u %15u %15u\n", builder.getSize(), builder.getMaximum(),
               builder.getLowestUnsaturatedDifference());
   return 0;
}
