#include "GDSBuilder.h"
#include <cstdlib>   // std::abs()


/****************/
/**** PUBLIC ****/
/****************/

GDSBuilder::GDSBuilder(unsigned int cap, Algorithm algorithm) {
   setCap(cap);
   setAlgorithm(algorithm);
   setStart(1);
}

/* Adds a new element (or two elements at once) to the set. The way this is
** accomplished depends on the algorithm that is currently being used.  */
bool GDSBuilder::addNext() {
   switch (this->algorithm_) {
      case GREEDY:
         return addNextGreedy();
      case SUPERGREEDY:
         return addNextSuperGreedy();
      case HYBRID:
         return addNextHybrid();
      case MODGREEDY:
         return addNextModGreedy();
   }
   return false;
}

/* Sets the algorithm to use to add new elements. */
void GDSBuilder::setAlgorithm(Algorithm algorithm) {
   this->algorithm_ = algorithm;
}

/* Sets the starting element of the set (but only if it's a positive integer
** and no element has been added yet). */
void GDSBuilder::setStart(unsigned int start) {
   if (this->gds_.empty() && start > 0) {
      this->start_ = start;
      setFrequencySequence();
   }
}

/* Sets the maximum supported value of a set element (only available prior to
** adding any elements). */
void GDSBuilder::setCap(unsigned int cap) {
   if (this->gds_.empty() && cap > 0) {
      this->cap_ = cap;
      setFrequencySequence();
   }
}

/* Sets the initial frequency sequence. */
void GDSBuilder::setFrequencySequence() {
   this->differences_.assign(this->cap_ - this->start_ + 1, 1);
   this->min_diff_ = 1;
   this->intervention_counter_ = 0;
}

/* Sets the intervention point used in the hybrid algorithm: the number of
** iterations the lowest unsaturated difference has to remain unchanged to
** be manually added to the set. */
void GDSBuilder::setInterventionPoint(unsigned int iterations) {
   this->intervention_point_ = iterations;
}

/* Returns the algorithm that is currently being used to add new elements. */
Algorithm GDSBuilder::getAlgorithm() const {
   return this->algorithm_;
}

/* Returns the name of the currently used algorithm. */
std::string GDSBuilder::getAlgorithmName() const {
   switch (this->algorithm_) {
      case GREEDY:
         return "greedy";
      case SUPERGREEDY:
         return "supergreedy";
      case HYBRID:
         return "hybrid";
      case MODGREEDY:
         return "modified greedy";
   }
   return "unknown";
}

/* Returns the first element to be added to the set. */
unsigned int GDSBuilder::getStart() const {
   return this->start_;
}

/* Returns the maximum supported value of an element. */
unsigned int GDSBuilder::getCap() const {
   return this->cap_;
}

unsigned int GDSBuilder::getInterventionPoint() const {
   return this->intervention_point_;
}

/* Returns the maximum element in the set at the moment. */
unsigned int GDSBuilder::getMaximum() const {
   if (!this->gds_.empty()) {
      return this->gds_.back();
   }
   return 0;
}

/* Returns the current number of elements in the set. */
unsigned int GDSBuilder::getSize() const {
   return this->gds_.size();
}

/* Returns the first (lowest) unsaturated difference size. */
unsigned int GDSBuilder::getLowestUnsaturatedDifference() const {
   return this->min_diff_;
}


/*****************/
/**** PRIVATE ****/
/*****************/

/* Adds the provided element to the set (if it doesn't exceed cap_) and
** updates all differences created by doing so. */
bool GDSBuilder::add(unsigned int element) {
   if (element <= this->cap_) {
      this->gds_.push_back(element);
      addDifferencesFrom(element);
      return true;
   }
   return false;
}

/* Finds a pair of elements to add to the set using greedy. Returns true if
** they have been added successfully. */
bool GDSBuilder::addNextGreedy() {
   unsigned int min_diff = getLowestUnsaturatedDifference();
   unsigned int next;
   if (this->gds_.empty()) {
      next = this->start_;
   }
   else {
      next = getMaximum() + min_diff;
   }

   while (next + min_diff <= this->cap_) {
      if (!violatesFrequencySequence(next, next + min_diff)) {
         return add(next) && add(next + min_diff);
      }
      next++;
   }
   return false;
}

/* Finds and adds a new element to the set using supergreedy. */
bool GDSBuilder::addNextSuperGreedy() {
   unsigned int next = getMaximum() + getLowestUnsaturatedDifference();
   while (next <= this->cap_ && violatesFrequencySequence(next)) {
      next++;
   }
   return add(next);
}

/* Finds and adds a new element using the supergreedy algorithm. If the
** current lowest unsaturated diffence remains unsaturated for a long time,
** the algorithm switches to greedy to add it and then continues with
** supergreedy. */
bool GDSBuilder::addNextHybrid() {
   if (this->intervention_counter_ >= getInterventionPoint()) {
      this->intervention_counter_ = 0;
      return addNextGreedy();
   }
   else {
      unsigned int old_lowest = this->min_diff_;
      unsigned char old_frequency = this->differences_[this->min_diff_];
      bool success = addNextSuperGreedy();
      if (!success) {
         return false;
      }
      if (this->min_diff_ == old_lowest &&
          this->differences_[this->min_diff_] == old_frequency) {
         this->intervention_counter_++;
      }
      else {
         this->intervention_counter_ = 0;
      }
      return true;
   }
}

/* Finds and adds a new element (or elements) according to the modified
 * greedy algorithm. First checks whether the lowest unsaturated difference
 * cannot be added using just one element and if that fails, turns to
 * the greedy algorithm. */
bool GDSBuilder::addNextModGreedy() {
   if (getMaximum() + this->min_diff_ >= this->cap_) {
      return false;
   }
   if (!violatesFrequencySequence(getMaximum() + this->min_diff_)) {
      return add(getMaximum() + this->min_diff_);
   }
   return addNextGreedy();
}

/* Finds all differences created by adding new_element to the set and updates
** their count. Also updates the lowest unsaturated difference size. */
void GDSBuilder::addDifferencesFrom(unsigned int new_element) {
   for (auto elem = this->gds_.cbegin(); elem != this->gds_.cend(); elem++) {
      if (*elem != new_element) {
         int diff = new_element - *elem;
         this->differences_[std::abs(diff)]--;
      }
   }
   updateLowestUnsaturatedDifference();
}

/* Finds the lowest unsaturated difference at the moment. */
void GDSBuilder::updateLowestUnsaturatedDifference() {
   for (auto diff = this->differences_.cbegin() + this->min_diff_;
        diff != this->differences_.cend(); diff++) {
      if (*diff > 0) {
         this->min_diff_ = diff - this->differences_.cbegin();
         break;
      }
   }
}

/* Finds out whether the provided difference can be added count times without
** violating the original frequency sequence. The count parameter defaults to
** one. */
bool GDSBuilder::checkDifference(unsigned int difference,
                                 unsigned char count) const {
   return this->differences_[difference] >= count;
}

/* Finds out whether the provided element can be added to the set without
** creating differences that do not appear in the original frequency sequence.
** */
bool GDSBuilder::violatesFrequencySequence(unsigned int element) const {
    for (auto elem = this->gds_.cbegin(); elem != this->gds_.cend(); elem++) {
       if (*elem != element && !checkDifference(element - *elem)) {
          return true;
       }
    }
    return false;
}

/* Finds out whether the addition of element1 and element2 to the set would
** create differences that are not present in the original frequency sequence.
** */
bool GDSBuilder::violatesFrequencySequence(unsigned int element1,
                                           unsigned int element2) const {
   // TODO: refactor
   if (getSize() == 0) {
      int diff = element2 - element1;
      return !checkDifference(std::abs(diff));
   }

   std::vector<unsigned int> differences1;
   differences1.assign(getSize(), 0);
   std::vector<unsigned int> differences2;
   differences2.assign(getSize() + 1, 0);
   differences2[0] = element2 - element1;

   unsigned int i = 0;
   for (auto elem = this->gds_.crbegin(); elem != this->gds_.crend(); elem++) {
      differences1[i] = element1 - *elem;
      differences2[i + 1] = element2 - *elem;
      i++;
   }

   auto pointer = differences2.begin();
   for (auto diff = differences1.cbegin(); diff != differences1.cend(); diff++) {
      bool twice = false;

      for (auto diff2 = pointer; diff2 != differences2.end() && *diff2 < *diff;
           diff2++) {
         pointer++;
      }

      if (*pointer == *diff) {
         twice = true;
         *pointer = 0;
         pointer++;
      }

      if (!checkDifference(*diff, (unsigned char) twice + 1)) {
         return true;
      }
   }

   for (auto diff = differences2.cbegin(); diff != differences2.cend();
        diff++) {
      if (*diff != 0 && !checkDifference(*diff)) {
         return true;
      }
   }
   return false;
}
