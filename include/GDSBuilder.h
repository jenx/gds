#ifndef GDSBUILDER_H
#define GDSBUILDER_H

#include <deque>
#include <string>
#include <vector>


enum Algorithm {GREEDY,
                SUPERGREEDY,
                HYBRID,
                MODGREEDY};


class GDSBuilder {
   public:
      GDSBuilder(unsigned int, Algorithm = GREEDY);
      virtual ~GDSBuilder() = default;

      bool addNext();

      void setAlgorithm(Algorithm);
      void setStart(unsigned int);
      void setCap(unsigned int);
      void setFrequencySequence();
      void setInterventionPoint(unsigned int);

      Algorithm getAlgorithm() const;
      std::string getAlgorithmName() const;
      unsigned int getStart() const;
      unsigned int getCap() const;
      unsigned int getInterventionPoint() const;
      unsigned int getMaximum() const;
      unsigned int getSize() const;
      unsigned int getLowestUnsaturatedDifference() const;
   protected:
   private:
      std::deque<unsigned int> gds_;
      std::vector<unsigned char> differences_;
      unsigned int cap_;
      unsigned int start_;
      unsigned int min_diff_;
      unsigned int intervention_counter_;
      unsigned int intervention_point_;
      Algorithm algorithm_;

      bool add(unsigned int);
      bool addNextGreedy();
      bool addNextSuperGreedy();
      bool addNextHybrid();
      bool addNextModGreedy();

      void addDifferencesFrom(unsigned int);
      void updateLowestUnsaturatedDifference();

      bool checkDifference(unsigned int, unsigned char = 1) const;
      bool violatesFrequencySequence(unsigned int) const;
      bool violatesFrequencySequence(unsigned int, unsigned int) const;
};

#endif
