Running GDSBuilder using supergreedy.

            |S|          max(S)     lowest diff

              2               2               2
              4               8               5
              8              45              15
             16             252              28
             32            1523              33
             64            8730              33
            128           52764              33
            256          336642              33
            512         2228833              33

Cap 10000000 exceeded; stopping.

            886         9991308              33

real    0m2.254s
user    0m2.250s
sys     0m0.003s
